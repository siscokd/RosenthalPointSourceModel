
import numpy as np
import matplotlib.pyplot as plt
from Rosenthal_Class import Rosenthal_Model

plt.close('all')

def HeatMap(x, y , z, Label, vmin, vmax):
    
    fig, ax = plt.subplots() # Creates your figure

    pc = ax.pcolormesh(x,y,z, vmin = vmin, vmax = vmax, cmap = 'coolwarm')

    cbar = fig.colorbar(pc)
    cbar.set_label(Label)
    ax.set_xlabel('Distance (microns) x direction')
    ax.set_ylabel('Distance (microns) z direction')
    ax.set_xlim([-100*1e-6,25*1e-6])
    plt.axis('scaled')
    
    plt.show()
    return







T0 = 23 + 273 # Room Temperature (K)
Eta = 0.33 # Assumed Absorption Efficiency
P = 200 # Input Power (W)
v = 1 # Laser Velocity in (100) direction (m/s)
k = 175.0 # Thermal Conductivity (W/m2K)
alpha = 7.1e-5 # Thermal Diffusivity (m2/s)
Liquidus_Temperature = 927
numPoints = 20

X = np.linspace(-150,25,numPoints)*1e-6 # x values between 0 and 100 microns
Y = np.linspace(-100, 100, numPoints)*1e-6 # y values = 0
Z = -1e-6 # z values = 0



myModel = Rosenthal_Model(velocity = v, Laser_Power = P, efficiency = Eta, temperature_initial = T0,
                          conductivity = k, thermal_diffusivity = alpha, Liquidus = Liquidus_Temperature )



X_Values = np.zeros((numPoints,numPoints))
Y_Values = np.zeros((numPoints,numPoints))
Z_Values = np.zeros((numPoints,numPoints))


Temperatures = np.zeros((numPoints, numPoints))
Thermal_Gradients = np.zeros((numPoints,numPoints))
Solidification_Velocitys = np.zeros((numPoints,numPoints))


for itr, x in enumerate(X):
    print(itr)
    for jtr, y in enumerate(Y):
        
        _, _ , z, Temperature, Thermal_Gradient, Solidification_Velocity = myModel.Calcualte_G_and_V(x, y, -1e-6)
        
        
        X_Values[itr,jtr] = x
        Y_Values[itr,jtr] = y
        Z_Values[itr,jtr] = z
        
        
        Temperatures[itr,jtr] = Temperature
        Thermal_Gradients[itr,jtr] = Thermal_Gradient
        Solidification_Velocitys[itr,jtr] = Solidification_Velocity

    

Solidification_Velocitys[Z_Values > -1e-6] = np.nan
Solidification_Velocitys[Solidification_Velocitys < 0] = np.nan
Thermal_Gradients[np.isnan(Solidification_Velocitys)] = np.nan
Z_Values[np.isnan(Solidification_Velocitys)] = np.nan

HeatMap(x = X_Values*1e6, y = Y_Values*1e6, z = Z_Values*1e6, Label = 'Depth (um)', vmin = -40, vmax = 0)

HeatMap(x = X_Values*1e6, y = Y_Values*1e6, z = np.log10(Thermal_Gradients), 
        Label = 'Thermal Gradient (K/m)', vmin = 6, vmax = 8)

HeatMap(x = X_Values*1e6, y = Y_Values*1e6, z = np.log10(Solidification_Velocitys), 
        Label = 'Solidification Velocties (m/s)', vmin = -3, vmax = 1)


X_Values = X_Values[~np.isnan(Solidification_Velocitys)] 
Y_Values = Y_Values[~np.isnan(Solidification_Velocitys)] 
Z_Values = Z_Values[~np.isnan(Solidification_Velocitys)] 
Thermal_Gradients = Thermal_Gradients[~np.isnan(Solidification_Velocitys)] 
Solidification_Velocitys= Solidification_Velocitys[~np.isnan(Solidification_Velocitys)] 

Arrays = [np.ravel(X_Values),np.ravel(Y_Values),np.ravel(Z_Values),np.ravel(Solidification_Velocitys),np.ravel(Thermal_Gradients)]

np.savetxt('Testing.csv', np.transpose(Arrays), delimiter=',', header='x,y,z,V,G')


"""
New Plot that compares the Solidfication velocity and thermal gradients directly.
"""

plt.figure()
plt.scatter(Solidification_Velocitys, Thermal_Gradients)
plt.ylabel('Thermal Gradients (K/m)')
plt.xlabel('Solidification Velocities (m/s)')
plt.xscale('log')
plt.yscale('log')
plt.xlim([1e-5,10])
plt.ylim([1e5,1e9])
plt.show()
        
        
        
        
        
        
        
        
        


