# RosenthalPointSourceModel

The RosenthalPointSourceModel is an implimentation of the classical Rosenthal Equation that is used to approximate heat transfer in a weld track. The current implimentation demonstrates a way to predict solidification velocity and thermal gradient (R, G) as a function of processing conditions and material inputs.
