import numpy as np
from scipy.misc import derivative
from scipy import optimize

class Rosenthal_Model:
    """
    This class is used to calcualte the rosenthal model
    """
    def __init__(self, velocity, Laser_Power, efficiency, temperature_initial, conductivity, thermal_diffusivity, Liquidus):
        """The initition of the rosenthal class that take key model parameters

        Args:
            velocity (float): Speed of the heat source in the [1 0 0] [x, y, z] direction (m/s)
            Laser_Power (float): Input Power from the heat source (W)
            efficiency (float): the calibration term for power absorbed 
            temperature_initial (float): The temperature of the plate material (K)
            conductivity (float): the thermal conductivity of the plate material
            thermal_diffusivity (float): the thermal diffusivity of the plate material 
        """
        self.velocity = velocity
        self.Laser_Power = Laser_Power
        self.efficiency = efficiency
        self.temperature_initial = temperature_initial
        self.conductivity = conductivity
        self.thermal_diffusivity = thermal_diffusivity
        self.Liquidus = Liquidus
        
    def Rosenthal_Temperature_Calculation(self, x,y,z):
        """Calcualtes the Temperature at a give point by the rosenthal model

        Args:
            x (float): x-coordinate (m)
            y (float): y-coordinate (m)
            z (float): z-coordinate (m)

        Returns:
            float: Temperature
        """
        r = (x**2.0 + y**2.0 + z**2.0)**(1.0/2.0)
        temperature = self.temperature_initial + (self.efficiency*self.Laser_Power/(2*np.pi*self.conductivity*r))  *  np.exp((-self.velocity/(2*self.thermal_diffusivity))*(r+x))
        return temperature
    
    def rosenthal_eq_X(self, x, y, z):
        temperature= self.Rosenthal_Temperature_Calculation(x,y,z)
        return temperature

    def rosenthal_eq_Y(self, y, x, z):
        temperature= self.Rosenthal_Temperature_Calculation(x,y,z)      
        return temperature

    def rosenthal_eq_Z(self, z, x, y):
        temperature= self.Rosenthal_Temperature_Calculation(x,y,z)
        return temperature

    def Deriv_Rosenthal_Eq_X(self, x, y, z):
        X_Slope = derivative(self.rosenthal_eq_X, x, dx =1e-6, args= (y, z))
        return X_Slope

    def Deriv_Rosenthal_Eq_Y(self, y, x, z):
        Y_Slope = derivative(self.rosenthal_eq_Y, y, dx =1e-6, args= (x, z))
        return Y_Slope

    def Deriv_Rosenthal_Eq_Z(self, z, x, y):
        Z_Slope = derivative(self.rosenthal_eq_Z, z, dx =1e-6, args= (x, y))
        return Z_Slope
     
    def rosenthal_eq_Solve(self, z, x, y):
        temperature = self.Rosenthal_Temperature_Calculation(x,y,z)
        return self.Liquidus - temperature
 
    def Solve_Z_Rosenthal(self, x, y, z):
        ## The Bounds here are arbirutary
        return optimize.least_squares(self.rosenthal_eq_Solve, z, args= (x, y,), bounds = (-800, -1e-7))
     
    def rosenthal_eq(self, x, y, z):
        """Calcualtes the Temperature at a give point by the rosenthal model
        as well as the parital derivatives in the x,y, and z directions for temperature.

        Args:
            x (float): x-coordinate (m)
            y (float): y-coordinate (m)
            z (float): z-coordinate (m)

        Returns:
            float: Temperature
        """
        temperature= self.Rosenthal_Temperature_Calculation(x,y,z)
        
        Parital_X = self.Deriv_Rosenthal_Eq_X(x, y, z)
        Partial_Y = self.Deriv_Rosenthal_Eq_Y(y, x, z)
        Partial_Z = self.Deriv_Rosenthal_Eq_Z(z, x, y)

        return temperature, Parital_X, Partial_Y, Partial_Z

    def Calcualte_G_and_V(self, x,y,z):
        """
        Determines the thermal gradient and solidifiation velocity at each provided point.
        This is done by assuming an X and Y value and guessing a Z value. Then the actual Z value
        is determined.
        Args:
            x (float): actual x-coordinate (m)
            y (float): actual y-coordinate (m)
            z (float): guess z-coordinate (m)

        Returns:
            x (float): x-coordinate (m)
            y (float): y-coordinate (m)
            z (float): z-coordinate (m)
            float: Temperature
            float: Thermal_Gradient
            float: Solidification_Velocity 
        """

        z = (self.Solve_Z_Rosenthal(x, y, z))['x']
        Temperature, Px,Py,Pz = self.rosenthal_eq(x, y, z)           

        Velocity_Vector = np.array([self.velocity, 0 , 0])
        Normal = np.array([Px,Py,Pz])/(np.sqrt(Px**2+Py**2+Pz**2))
        Solidification_Velocity = np.dot(Velocity_Vector,Normal)
        Thermal_Gradient = np.sqrt(Px**2+Py**2+Pz**2)

        return x,y,z, Temperature, Thermal_Gradient, Solidification_Velocity

   
